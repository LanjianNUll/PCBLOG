/*
 * @Author: lanjian
 * @Date: 2018-09-10 10:42:45
 * @Last Modified by: lanjian
 * @Last Modified time: 2018-09-12 16:19:51
  * @Content desc:
 */

const config = {
    blogSortList:[
        {sortName:"关注",whichTab:1,interface:"getFocusOnList"},
        {sortName:"全局",whichTab:2,interface:"getAllBlog"},
        {sortName:"二级单位",whichTab:3,interface:"getDeptBlogList"},
        {sortName:"三级单位",whichTab:4,interface:"getDeptBlogList"},
        {sortName:"白金",whichTab:5,interface:"getPlatinumList"},
        {sortName:"精华",whichTab:6,interface:"getCreamList"},
        {sortName:"话题",whichTab:7,interface:""},
        {sortName:"收藏",whichTab:8,interface:"getCollectionList"}
    ],
    searchPlaceholder:"搜索",
    alwaysOnTopTips:"你设置了保持警信窗口在最前面，请在主界面取消",
    typeTagList:[
        {
            //普通动态
            tagTypeId: "164c3ef0f80546959783c39a24eaae3d",
            tagTypeName: "问题建议"
        },
        {   //美篇风格
            tagTypeId: "8c9a7a8123ba41b9a10bd5f235435880",
            tagTypeName: "经验心得"
        },
        {
            //美篇风格
            tagTypeId: "6c345051256a4fec97e106b4a7e10efd",
            tagTypeName: "工作动态"
        }, {
            //美篇风格
            tagTypeId: "c28ebb9bf24346eb817de371e22925c3",
            tagTypeName: "生活感悟"
        }, {
            //普通动态
            tagTypeId: "5ed112d43d8d471fac702917c99ed15a",
            tagTypeName: "知识问答"
        }, {
            //普通动态
            tagTypeId: "e5bb47cd09634d28ac66174d420e2944",
            tagTypeName: "营养热帖"
        }
    ]
};

export default config;