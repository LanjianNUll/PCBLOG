import React,{Component} from 'react';
import ReactDOM from 'react-dom';
import './style/index.css'
import WrappedNormalLoginForm from './components/loginForm';

class Login extends Component {
  render() {
    return (
      <div className="loginC">
        <WrappedNormalLoginForm />
      </div>
    );
  }
}

ReactDOM.render(<Login />, document.getElementById('root'));





