/*
 * @Author: lanjian
 * @Date: 2018-09-07 10:05:33
 * @Last Modified by: lanjian
 * @Last Modified time: 2018-09-14 20:30:48
 * @Content desc:入口文件类
 */

import React from 'react';
import ReactDOM from 'react-dom';
import './style/index.css';
import App from './container/App';
import registerServiceWorker from './registerServiceWorker';
import {BrowserRouter} from 'react-router-dom';

ReactDOM.render(
    <BrowserRouter>
    <App />
  </BrowserRouter>,
 document.getElementById('root'));
registerServiceWorker();
