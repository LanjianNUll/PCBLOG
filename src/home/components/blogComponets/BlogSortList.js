/*
 * @Author: lanjian
 * @Date: 2018-09-10 11:30:17
 * @Last Modified by: lanjian
 * @Last Modified time: 2018-09-18 17:42:23
  * @Content desc:  动态pc动态分页
 */

import React ,{ Component} from 'react';
import { Input } from 'antd';
import classnames from 'classnames';
import { uiConfig } from '../../../config/index';

const Search = Input.Search;

class BlogSortList extends Component{

    constructor(props){
        super(props);
        this.state = {
            sortList:uiConfig.blogSortList,
            currentActive: 2
        }
        this.searchForBlog = this.searchForBlog.bind(this);
    }

    render(){
        const listItems = this.state.sortList.map((item,index) =>
            <li key={index} className={classnames({
                    "sortListItemLi":true,
                    "sortListItmeActive":(this.state.currentActive===(index+1))
            })} onClick={this.sortTabClick.bind(this,item.whichTab,item)}>
                {item.sortName}
            </li>
        );
        return (
            <div className="blogListDiv">
                <Search className="search"
                placeholder={uiConfig.searchPlaceholder}
                onSearch={this.searchForBlog}
                    />
                <ul className="sortListUi">{listItems}</ul>
            </div>
        );
    }

    searchForBlog(value){
        console.log(value);
    }

    sortTabClick(index,item){
        this.setState({
            currentActive: index
        });
        var tempObj = {
            title:item.sortName,
            index:index
        };
        this.props.sortListClick(tempObj);
    }
}

export default BlogSortList;