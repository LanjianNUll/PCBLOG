/*
 * @Author: lanjian
 * @Date: 2018-09-11 19:31:07
 * @Last Modified by: lanjian
 * @Last Modified time: 2018-09-11 19:31:4111
  * @Content desc: 动态评论组件
 */

 import React,{Component} from 'react';
 import classNames from 'classnames';
 import _ from 'lodash';
 import {Link} from 'react-router-dom';

 export default class BlogComments extends Component{
    constructor(props){
        super(props);
        this.state = {
            itemData:this.props.data,
            isDetail:(!!this.props.data.isDetail)?true:false
        }
        //详情要重新获取评论，热门评论和最新评论

    }

   // 父组件更新props的变化
    componentWillReceiveProps(nextProps){
        this.setState ({
            itemData:nextProps.data
        });
    }

    render(){
        let tempArr = _.slice(this.state.itemData.commentList,0,6);
        //详情显示
        if(!!this.state.isDetail){
            tempArr = this.state.itemData.commentList;
        }
        let fristCommentList = "";
        fristCommentList = tempArr.map((item,index) =>{
            return (<div key={index} className="firstCommentItem">
                        <CommentItemC  data={item}/>
                        <div></div>
                    </div>);
        });
        let moreCommentS = "";
        if(this.state.itemData.commentList > 6){
            moreCommentS =  <Link to={`/detail/${this.state.itemData.blogId}`}>
                                <div>共有{this.state.itemData.commentList.length}条评论</div>
                            </Link>
        }
        //详情显示
        if(!!this.state.isDetail){
            moreCommentS = "";
        }
        return (
            <div className={classNames({
                "blogcommentDiv":(this.state.itemData.commentList.length > 0)
                })}>
                {fristCommentList}
                {moreCommentS}
            </div>
        )
    }

    openDetail(){
        console.log("打开详情");
    }
}

class CommentItemC extends Component{
    constructor(props){
        super(props);
        this.state = {
            itemData:this.props.data
        }
    }

    render(){
        return (
            <div className="blogcommentItemDiv">
                <span className={classNames({
                    "textBlue":true,
                    "textRed":true
                })}>{this.state.itemData.userName}</span> ：
                {this.state.itemData.content}
            </div>)
    }
}
