/*
 * @Author: lanjian
 * @Date: 2018-09-11 19:31:07
 * @Last Modified by: lanjian
 * @Last Modified time: 2018-09-11 19:31:4111
  * @Content desc: 动态主体头部
 */

 import React,{Component} from 'react';
 import _ from 'lodash';
 import {uiConfig,netConfig} from '../../../../config/index'
 import { Menu, Dropdown, Icon } from 'antd';


 export default class BlogHeader extends Component{

    constructor(props){
        super(props);
        this.state = {
            itemData:this.props.data
        }
        this.setCreamBlog = this.setCreamBlog.bind(this);
        this.collectBlog = this.collectBlog.bind(this);
        this.copyBlog = this.copyBlog.bind(this);
    }

    //父组件更新props的变化
    componentWillReceiveProps(nextProps){
        this.setState ({
            itemData:nextProps.data,
        })
    }

    render(){
        const menu = (
            <Menu>
              <Menu.Item onClick={this.setCreamBlog}>
                {(!!this.state.itemData.isCream)?"取消精华":"添加精华"}
              </Menu.Item>
              <Menu.Item onClick={this.collectBlog}>
              {(!!this.state.itemData.collect)?"取消收藏":"收藏动态"}
              </Menu.Item>
              <Menu.Item onClick={this.copyBlog}>
                复制动态
              </Menu.Item>
            </Menu>
          );

        return (
            <div className="blogHeader">
                <div className="headerContent">
                    <div className="headImgDiv">
                        {(!!this.state.itemData.avatarUrl)
                            ?<img alt="头像" className="headImg" src={netConfig.preHead+this.state.itemData.avatarUrl}/>
                            :<div className="headName">{this.state.itemData.userName[this.state.itemData.userName.length-1]}</div>}
                    </div>
                    <span className="name">{this.state.itemData.userName}</span>
                    {(!!this.state.itemData.isPlatinum)?<span className="stateTag">白金</span>:""}
                    {(!!this.state.itemData.isCream)?<span className="stateTag">精华</span>:""}
                    <div className="typeTag">{(_.find(uiConfig.typeTagList,
                     (item) => {
                        return item.tagTypeId === this.state.itemData.tagTypeId;
                        })).tagTypeName}</div>
                </div>
                <Dropdown className="headDropdown" overlay={menu}>
                     < Icon className="dropIcon" type="ellipsis" />
                </Dropdown>
            </div>
        )
    }

    setCreamBlog(){
        console.log("setCreamBlog");
    }

    collectBlog(){
        console.log("collectBlog");
    }

    copyBlog(){
        console.log("copyBlog");
    }
}
