/*
 * @Author: lanjian
 * @Date: 2018-09-11 19:31:07
 * @Last Modified by: lanjian
 * @Last Modified time: 2018-09-11 19:31:4111
  * @Content desc: 动态点赞行组件
 */

import React,{Component} from 'react';
import classNames from 'classnames';
import {Icon} from 'antd';
import _ from 'lodash';
import {Link} from 'react-router-dom';

export default class BlogThumps extends Component{

    constructor(props){
        super(props);
        this.state = {
            itemData:this.props.data,
            isThumpUp:this.props.data.isThumpUp,
            thumpUpCount:this.props.data.thumpUpCount,
            isDetail:(!!this.props.data.isDetail)?true:false
        }
    }

    //父组件更新props的变化
    componentWillReceiveProps(nextProps){
        this.setState ({
            itemData:nextProps.data,
            isThumpUp:nextProps.data.isThumpUp,
            thumpUpCount:nextProps.data.thumpUpCount,
            isDetail:(!!nextProps.data.isDetail)?true:false
        });
    }

    render(){
        let tempList = "";
        let personCount = "";
        let detailPersonCount = "";
        if(this.state.thumpUpCount > 0){
            let tempArr = _.slice(this.state.itemData.thumpUpPeople,0,10);
            //详情显示
            if(!!this.state.isDetail){
                tempArr = this.state.itemData.thumpUpPeople;
            }
            tempList = tempArr.map((item,index) =>{
                let splicTag = "";
                if(index !== tempArr.length - 1){
                    splicTag = ",";
                }
                return (<span className="textBlue" key={index}>{item.thumpUpUserName} {splicTag}</span>);
            });
            if(this.state.itemData.thumpUpCount > 8 ){
                personCount = this.state.itemData.thumpUpCount+"人点赞";
            }
            //详情显示
            if(!!this.state.isDetail){
                personCount = "共"+this.state.itemData.thumpUpCount+"人";
            }
            //详情显示
            if(!!this.state.isDetail){
                detailPersonCount = <span className="textBlue" > 共{this.state.itemData.thumpUpCount}人</span>
                personCount = "";;
            }
        }
        return (
            <div className={classNames({"thumpLists":(this.state.thumpUpCount > 0)})}>
                {(this.state.thumpUpCount > 0)?<Icon type="like" className="textBlue" theme="filled"/>:""}
                {tempList}
                <Link to={`/home/blogdetail/${this.state.itemData.blogId}`}><span className="textBlue" >{personCount}</span></Link>
                {detailPersonCount}
            </div>
        )
    }
}
