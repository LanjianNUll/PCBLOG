/*
 * @Author: lanjian
 * @Date: 2018-09-11 19:31:07
 * @Last Modified by: lanjian
 * @Last Modified time: 2018-09-11 19:31:4111
  * @Content desc: 动态图片展示组件
 */

import React,{Component} from 'react';
import _ from 'lodash';
const Electron = window.electron;

export default class BlogImageBody extends Component{

    constructor(props){
        super(props);
        this.state={
            itemData:this.props.data.blogAttachments,
        }
        this.picClick = this.picClick.bind(this);
    }

    //父组件更新props的变化
    componentWillReceiveProps(nextProps){
        this.setState ({
            itemData:nextProps.data.blogAttachments,
        })
    }

    render(){
        let listItems = "";
        if(!!this.state.itemData){
            listItems = this.state.itemData.map((item,index) =>
                <img alt="" key={index} className="imgR"
                        onClick={this.picClick.bind(this,index)}
                        src={item.pictureUrl} />
            );
        }
        //一张图片同比例缩小
        if(!!this.state.itemData && this.state.itemData.length === 1){
            let tempObj = this.state.itemData[0];
            let width = tempObj.picWidth;
            let height = tempObj.picHight;

            if(!tempObj.picWidth || !tempObj.picHight){
                width = 125;
                height = 125;
            }
            width = 125;
            height = 125;
            listItems =
                <img alt="" className="imgR" onClick={this.picClick.bind(this,0)}
                src={tempObj.pictureUrl} style={{width:width,height:height}}/>
        }

        return (
            <div className="imageBody">
                {listItems}
            </div>
        )
    }

    //处理图片宽高
    dealWithWH(w){
        return w;
    }

    picClick(index){
        console.log("点击了第几张图。",index);
        Electron.ipcRenderer.send("openPicWindow",index,this.state.itemData);
    }

}
