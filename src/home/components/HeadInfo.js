/*
 * @Author: lanjian
 * @Date: 2018-09-07 10:00:03
 * @Last Modified by: lanjian
 * @Last Modified time: 2018-09-18 17:52:04
 * @Content desc:
 */
import React, {Component} from 'react';
import defaultHeadImg from '../../asset/defaultHead.jpg';
import { Popover } from 'antd';
import { netUtil } from '../../utils/index';
class HeadInfo extends Component{

    constructor(props){
        super(props);
        this.state={
            headImage:defaultHeadImg,
            userInfo:{
                userName:"姓名",
                userCode:"000101"
            }
        };
    }

    componentDidMount(){
        netUtil.fetchPost("/moments/user/ui/getUserInfoByRedis",
            {
                userId: "3dfcbe3d-8a5d-47fc-9dba-7c8af8d7d50e",
                isValidate: false
            },
            (res)=>{
                console.log("数据",res);
                this.setState({userInfo:res.data,headImage:res.data.jmtAvatarUrl});
                window.userData = res.data;
        })
    }

    render(){
        const content = (
            <div className="popDiv">
              <div className="upPopDiv">
                <div className="userInfoDiv">
                    <div className="userName">{this.state.userInfo.userName}</div>
                    <div className="policNum">警号:{this.state.userInfo.userCode}</div>
                </div>
                <div className="userImgDiv">
                    <img alt="头像" src={this.state.headImage} className='userInfoImg'/>
                </div>
              </div>
              <div className="downPopDiv">

              </div>
            </div>
          );
        return(
            <Popover placement="rightTop" title={""} content={content} trigger="click">
                <div className="headImageDiv">
                    <img alt="头像" src={this.state.headImage} className='headImg'/>
                </div>
            </Popover>
        )
    }
}

export default HeadInfo;
