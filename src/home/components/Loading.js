/*
 * @Author: lanjian
 * @Date: 2018-09-17 18:01:01
 * @Last Modified by: lanjian
 * @Last Modified time: 2018-09-17 18:10:50
 * @Content desc:  全局的加载组件
*/
import React ,{Component} from 'react';
import {Icon} from 'antd';

class Loading extends Component{

    render(){
        return(
            <div className="lodingDiv">
               <Icon type="loading" theme="outlined" />
               <div className="loadingFont">加载中...</div>
            </div>
        )
    }
}

export default Loading;
