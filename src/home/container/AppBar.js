/*
 * @Author: lanjian
 * @Date: 2018-09-07 10:01:58
 * @Last Modified by: lanjian
 * @Last Modified time: 2018-09-18 12:20:25
 * @Content desc:右边布局
 */

import React, { Component } from 'react';
import {Icon} from 'antd';
import classnames from 'classnames';
import HeadInfo from '../components/HeadInfo';
import {Link} from 'react-router-dom';

class AppBar extends Component{

    constructor(props){
        super(props);
        this.state = {
            active:"blog",
            userInfo:""
        }
        this.blogClick = this.blogClick.bind(this);
        this.messageClick = this.messageClick.bind(this);
        this.collectionClick = this.collectionClick.bind(this);
    }

    render(){
        return (
            <div className="left">
                <HeadInfo userInfo={this.state.userInfo}/>
                <Link to="./">
                    <Icon type="home" theme="outlined"  onClick={this.blogClick}
                        className={classnames({
                            "blog":true,
                            "active":(this.state.active==="blog"?true:false)
                        })}
                    />
                </Link>
                <Link to="./message">
                    <Icon type="message" theme="outlined"  onClick={this.messageClick}
                        className={classnames({
                            "message":true,
                            "active":this.state.active === "message"?true:false
                        })}
                    />
                </Link>
                <Link to="./collection">
                    <Icon type="star" theme="outlined" onClick={this.collectionClick}
                        className={classnames({
                            "collection":true,
                            "active":this.state.active === "collection"?true:false
                        })}
                        />
                </Link>
            </div>
        )
    }

    blogClick(){
        this.setState({
            active:"blog"
        });
        this.props.barClick("blog");
    }

    messageClick(){
        console.log("messageClick");
        this.setState({
            active:"message"
        });
        this.props.barClick("message");
    }

    collectionClick(){
        console.log("collectionClick");
        this.setState({
            active:"collection"
        });
        this.props.barClick("collection");
    }
}

export default AppBar;