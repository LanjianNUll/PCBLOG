/*
 * @Author: lanjian
 * @Date: 2018-09-07 10:00:27
 * @Last Modified by: lanjian
 * @Last Modified time: 2018-09-14 20:44:46
  * @Content desc:App界面主体类
 */

import React, { Component } from 'react';
import '../style/App.css';
import Layout from './Layout';

class App extends Component {

  static defaultProps = {
    color: '#555',
    theme: 'light'
  };

  render() {
    return (
      <div className="App">
        <Layout />
      </div>
    );
  }
}

export default App;
