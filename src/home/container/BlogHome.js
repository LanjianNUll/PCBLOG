/*
 * @Author: lanjian
 * @Date: 2018-09-14 19:46:48
 * @Last Modified by: lanjian
 * @Last Modified time: 2018-09-17 11:09:11
  * @Content desc: 动态主页
 */

import React, { Component } from 'react';
import BlogBar from '../components/blogComponets/BlogBar';
import BlogListContent from '../components/blogComponets/BlogListContent';

class BlogHome extends Component {

  constructor(props){
      super(props);
      this.state = {
        rData:{
          disPlaytitle:"全局",
          index:2
        }
      }
      this.sortListClick = this.sortListClick.bind(this);
  }

  render() {
      return (
        <div className="Cparent">
            <BlogBar sortListClick={this.sortListClick}/>
            <BlogListContent rData={this.state.rData}/>
        </div>
      );
  }

  sortListClick(data){
    this.setState({
      rData:{
        disPlaytitle:data.title,
        index:data.index
      }
    })
  }
}

export default BlogHome;

