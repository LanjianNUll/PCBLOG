/*
 * @Author: lanjian
 * @Date: 2018-09-07 16:32:52
 * @Last Modified by: lanjian
 * @Last Modified time: 2018-09-07 16:56:322
  * @Content desc:网络请求方法
 */
import { netConfig } from '../config/index';

 const fetchPost = (url,params,callBack) =>{
     let urlPre = netConfig.officeUiServiceUrlPre;
     if(netConfig.dev){
        urlPre = netConfig.testUiServiceUrlPre;
     }
    fetch(urlPre+url,{
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(params)
    }).then(
        (result) => {
            if (result.ok) {
                result.json().then(
                    (data) => {
                        if(!!callBack){
                            callBack(data);
                        }
                    }
                )
            }
        }
    ).catch((error) => {
        console.log(error)
    });
 }

 const fetchJosn = (url,param,callBack) =>{

}

const fetchGet = (url,param,callBack) =>{

}

export default{
    fetchPost,
    fetchJosn,
    fetchGet
}


