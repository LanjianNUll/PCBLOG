/*
 * @Author: lanjian
 * @Date: 2018-09-18 19:02:39
 * @Last Modified by: lanjian
 * @Last Modified time: 2018-09-18 19:39:39
 * @Content desc:主窗口
*/

const {BrowserWindow} = require('electron')
const path = require('path')
const url = require('url')
const pkg = require('../package.json')
const {ipcMainResgiter} = require('../electron/ipcMainResgiter');
const {TrayAndMenu} = require('../electron/TrayAndMenu');

function createHomeWindow (data) {
    let win;
    // 创建浏览器窗口。
    win = new BrowserWindow({
      width: 960,
      height: 820,
      autoHideMenuBar: true,
      fullscreenable: false,
      webPreferences: {
          javascript: true,
          plugins: true,
          nodeIntegration: true,
          webSecurity: false,
          preload: path.join(__dirname, '../public/renderer.js') // 但预加载的 js 文件内仍可以使用 Nodejs 的 API
      },
      show: false,
      frame: false
    });

    // 然后加载应用的 index.html。
    // package中的DEV为true时，开启调试窗口。为false时使用编译发布版本
    if(pkg.DEV){
      win.loadURL('http://localhost:3000/home/#/layout')
    }else{
      win.loadURL(url.format({
        pathname: path.join(__dirname, './build/home/'),
        protocol: 'file:',
        slashes: true
      }))
    }

    // 打开开发者工具。
    win.webContents.openDevTools()

    // 当 window 被关闭，这个事件会被触发。
    win.on('closed', () => {
      // 取消引用 window 对象，如果你的应用支持多窗口的话，
      // 通常会把多个 window 对象存放在一个数组里面，
      // 与此同时，你应该删除相应的元素。
      win = null
    })

    win.once('ready-to-show', () => {
      win.show()
    })
    //等app ready完
    //注册主进程的相关方法
    ipcMainResgiter(win);
    //系统角标和菜单
    TrayAndMenu(win);
    return win;
  }

  module.exports = createHomeWindow;