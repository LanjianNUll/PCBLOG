/*
 * @Author: lanjian
 * @Date: 2018-09-17 19:11:20
 * @Last Modified by: lanjian
 * @Last Modified time: 2018-09-18 17:20:27
 * @Content desc:新建窗口
*/

const BrowserWindow = require('electron').BrowserWindow;
const path = require('path');
function openPicWindowFunc(win,index,data){
    console.log(index,data);


    //作为主界面窗口的子窗口
    let child = new BrowserWindow({
        parent: win,
        titiel:"图片",
        autoHideMenuBar: true,
        fullscreenable: false,
        webPreferences: {
            javascript: true,
            plugins: true,
            nodeIntegration: true,
            webSecurity: false,
            preload: path.join(__dirname, '../public/renderer.js') // 但预加载的 js 文件内仍可以使用 Nodejs 的 API
            },
        show: false,
        frame: false //全屏
    })
    // 打开开发者工具。
    child.webContents.openDevTools()
    child.loadURL('http://localhost:3000/picView');
    child.once('ready-to-show', () => {
        child.show();
        child.webContents.send('openPic',index,data)
    });
    return child;
}

module.exports = openPicWindowFunc;