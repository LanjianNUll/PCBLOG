/*
 * @Author: lanjian
 * @Date: 2018-09-07 10:13:41
 * @Last Modified by: lanjian
 * @Last Modified time: 2018-09-18 17:52:29
  * @Content desc:图标和菜单的处理
 */

const { Tray } = require('electron');
const { Menu ,app } = require('electron');
const path = require('path');
const {ipcMain} = require('electron');
function TrayAndMenu(win){

    let trayIcon = path.join(__dirname, '../src/asset/img');//app是选取的目录
    let appTray = new Tray(path.join(trayIcon, 'favicon.ico'));//favicon.ico是app目录下的ico文件
     //系统托盘右键菜单
    var trayMenuTemplate = [
        {
            label: '设置',
            click: function () {

            } //打开相应页面
        },
        {
            label: '意见反馈',
            click: function () {

            } //打开相应页面
        },
        {
            label: '关于',
            click: function () {
            } //打开相应页面
        },
        {
            label: '退出',
            click: function () {
                app.quit();
                app.quit();//因为程序设定关闭为最小化，所以调用两次关闭，防止最大化时一次不能关闭的情况
            }
        },
     ];
    const contextMenu = Menu.buildFromTemplate(trayMenuTemplate);
    //设置此托盘图标的悬停提示内容
    appTray.setToolTip('新警信');
    appTray.setContextMenu(contextMenu);
    appTray.on('click', () => {
        win.isVisible() ? win.hide() : win.show()
      })
}

module.exports = {TrayAndMenu};