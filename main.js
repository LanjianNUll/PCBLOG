/*
 * @Author: lanjian
 * @Date: 2018-09-07 10:06:01
 * @Last Modified by: lanjian
 * @Last Modified time: 2018-09-18 19:40:34
 * @Content desc: electron主进程
 */

const {app} = require('electron')
const {ipcMain} = require('electron');
const loginWindow = require('./electron/loginWindow');
const createHomeWindow = require('./electron/homeWindow');
// 保持一个对于 window 对象的全局引用，如果你不这样做，
// 当 JavaScript 对象被垃圾回收， window 会被自动地关闭
let loginWin;
// Electron 会在初始化后并准备
// 创建浏览器窗口时，调用这个函数。
// 部分 API 在 ready 事件触发后才能使用。
app.on('ready', function(){
  loginWin = loginWindow();
});

//主界面窗口
ipcMain.on('loginSuccess', function(e,data) {
  createHomeWindow(data);
});

// 当全部窗口关闭时退出。
app.on('window-all-closed', () => {
  // 在 macOS 上，除非用户用 Cmd + Q 确定地退出，
  // 否则绝大部分应用及其菜单栏会保持激活。
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  // 在macOS上，当单击dock图标并且没有其他窗口打开时，
  // 通常在应用程序中重新创建一个窗口。
  if (loginWin === null) {
    loginWin = loginWindow()
  }
})
